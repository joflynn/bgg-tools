# README

This is a simple application that reads from the BoardGameGeek.com API to get
a user's collection of rated board games. Then runs some statistics on that, 
and presents the data with some charts and tables.

It's hosted [here](https://bgg-tools.herokuapp.com/collection/joflynn)