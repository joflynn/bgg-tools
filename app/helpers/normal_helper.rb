module NormalHelper
    class Normal
        @@domain = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    
        def initialize(mean, stddev, scale)
            @mean = mean
            @variance = stddev ** 2
            p stddev
            if @variance >= 0
                @variance = 1
            end

            @scale = scale / f(mean)
            @range = []
            @@domain.each do |x|
                @range << f(x) * @scale
            end
        end

        def f(x)
            Math::exp(-(x - @mean)**2/(2*@variance))/Math::sqrt(2*Math::PI*@variance)
        end

        def to_s
            @range.to_s
        end
    end
end
