require 'net/http'

class BggController < ApplicationController
    @@start_tries = 10

    def index
    end

    def search
        redirect_to action: 'collection', id: params[:username]
    end

    def collection
        @username = params[:id] 

        tries = @@start_tries
        status = '202'
        while (status != '200' and tries > 0) do
            if ( tries < @@start_tries) 
                p tries.to_s + " Try".pluralize(tries) + " left"
                sleep(10)
            end
            url = URI.parse('https://www.boardgamegeek.com/xmlapi/collection/' + @username + '?rated=1')
            p url.to_s
            request = Net::HTTP::Get.new(url.to_s)
            http = Net::HTTP.new(url.host, url.port)
            http.use_ssl = (url.scheme == "https")
    
            response = http.request(request) 
            status = response.code
            p status
            tries -= 1
        end

        if status == '200'
            @collection = Nokogiri::XML(response.body)

            @game_ratings = {}
            @game_plays = {}
            @bgg_ratings = {}
            @ratings = {}
            @weighted = {}

            @values = []
            @bgg_values = []
            @deviations = {}
            @deviations_sorted = []

            0.upto(10) do |i|
                @ratings[i] = []
                @weighted[i] = 0
            end
        
            @links = {}

            @mean = 0
            
            @stddev = 0
            @bgg_mean = 0
            @bgg_stddev = 0
    
            @sum = 0
            @sumsq = 0
            @n = 0

            @bgg_sum = 0
            @bgg_sumsq = 0

            @weighted_sum = 0
            @weighted_sumsq = 0

            @plays = 0
            @message = ""

            @collection.css("item").each do |item|
                name = item.css("name").inner_html
                id = item.attribute("objectid").value
                rating = item.css("rating").attribute("value").value.to_f
                bgg_rating = item.css("rating bayesaverage").attribute("value").value.to_f
                plays = item.css("numplays").inner_html.to_i
    
                if rating 
                    @ratings[rating.floor] << name
                    @weighted[rating.floor] += plays
                    @values << rating
                    @bgg_values << bgg_rating
    
                    @game_ratings[name] = rating
                    @game_plays[name] = plays
                    @bgg_ratings[name] = bgg_rating
    
                    @sum += rating
                    @sumsq += rating ** 2
                    @n += 1

                    @bgg_sum += bgg_rating
                    @bgg_sumsq += bgg_rating ** 2

                    @weighted_sum += rating * plays
                    @weighted_sumsq += (rating * plays) ** 2

                    if bgg_rating > 0
                        @deviations[name] = (rating - bgg_rating)
                    end

                    @plays += plays
                end

                @links[name] = "<a href='https://boardgamegeek.com/boardgame/#{id}'>#{name}</a>"
            end

            if @n > 0
                @mean = @sum / @n
                if @mean > 0
                    @stddev = Math::sqrt(@sumsq / (@n-1) - @mean ** 2) 
                else
                    @stddev = 1
                end
    
                @bgg_mean = @bgg_sum / @n
                if @bgg_mean > 0 
                    @bgg_stddev = Math::sqrt(@bgg_sumsq / (@n-1) - @bgg_mean ** 2) 
                else
                    @bgg_stddev = 1
                end

                @weighted_mean = @weighted_sum / @plays
                if @weighted_mean > 0 
                    @weighted_stddev = Math::sqrt(@weighted_sumsq / (@plays-1) - @weighted_mean ** 2) 
                else
                    @weighted_stddev = 1
                end


                @sorted = @values.sort
                @median = @sorted[(@sorted.length / 2.0).floor]

                @bgg_sorted = @bgg_values.sort
                @bgg_median = @bgg_sorted[(@bgg_sorted.length / 2.0).floor]

                @deviations_sorted = @deviations.sort_by {|name, dev| dev}
            else
                @message += "#{@username} has not rated any games. "
            end

            if @plays > 0
                @plays_sorted = @game_plays.sort_by{|name, plays| plays}.reverse
            else
                @message += "#{@username} has not logged any plays. "
            end
    
        else
            @error = "No Response from BGG, please try again in a moment."
        end
    end
end
