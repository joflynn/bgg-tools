Rails.application.routes.draw do
    root to: 'bgg#index'
    post '/', to: 'bgg#search'
    get '/collection/:id', to: 'bgg#collection'
end
